import Cajonsito from "@/componentes/Cajon";
import Footer from "@/componentes/Footer";

import Nav from "@/componentes/Nav";
import Bar from "@/componentes/barra";
import { Container } from "@mui/material";
import Image from "next/image";
export default function Home() {
  return (
    <main >
    <Container>
         <div className="welc">
         <Nav tit="Welcome to the Enchanted Nightmare"></Nav>
         </div>
      <div className="cartel">
           <div className="cart1">
             <h3>Enter if you dare... <br />
             where terrors roam <br />
             free and the darkness <br />
             whispers secrets
             </h3>
           </div>
           <div className="cart2">
           <h4>
           WARNING FROM<br />
           LOST SOULS 
           </h4>
           <Image src="/img/pal.svg" alt="" width={54}height={45} />
           </div>
      </div>
      <div>
        <Bar></Bar>
      </div>
   </Container>
    <Container>
         <div className="welc">
         <Nav tit="Pick your nightmare......"></Nav>
         </div>
         <div className="carton">
             
             <div>
         <Cajonsito titulo="The Maiden of Sorrows" texto="Enter the chamber of the maiden of sorrows. Can you uncover the truth behind her tragedy and release her tormented soul from its eternal anguish?" minitext="Difficulty Level: Easy " dinero="10,000"gemas="AED 100" fondo="/img/fand1.svg" ></Cajonsito>
         <Cajonsito titulo="The Haunted Palace" texto="Step into the Haunted Palace, where ghosts roam its halls. Can you solve its mysteries and escape the spectral embrace?" minitext="Difficulty Level: Hard " dinero="30,000"gemas="AED 300"fondo="/img/fand3.svg"></Cajonsito>
             </div>
             <div >
          <Cajonsito titulo="The Cursed Lake" texto="Dive into the eerie lake, where shadows hide ancient secrets. Explore the chilling depths to uncover lost truths which will be key to your safe return to the surface.." minitext="Difficulty Level: Medium " dinero="20,000"gemas="AED 200"fondo="/img/fond2.svg"></Cajonsito>
         <Cajonsito titulo="Shipwreck of Lost Souls" texto="Brave the sinister shipwreck, haunted by lost souls. Navigate its haunted corridors and uncover the fate of its doomed crew....before you join them." minitext="Difficulty Level: Easy " dinero="10,000"gemas="AED 100"fondo="/img/fand4.svg"></Cajonsito>
            </div>
    
         </div>
         <Bar></Bar>
    </Container>
    <Container>
    <div className="welc">
         <Nav tit="About The Enchanted Nightmare"></Nav>
    </div>
    <div>
      <p className="sectres">
      Welcome to The Enchanted Nightmare, where darkness reigns and nightmares come to life. <br /> We are not your typical escape room venue; we are purveyors of terror, architects of fear,<br /> and guardians of the macabre. 
      Within these walls, youll find a labyrinth of twisted tales and haunted horrors, each room a <br /> gateway to the darkest recesses of the human psyche.
      Our team is comprised of seasoned storytellers, masterful illusionists, and devoted disciples <br /> of the supernatural. We craft immersive experiences that blur the lines between reality and <br />nightmare, drawing you into a world where terror lurks around every corner and every <br /> step could be your last.
      Prepare to confront your deepest fears, as you navigate through our sinister chambers and <br /> face the horrors that dwell within. But be warned: once you enter The Enchanted <br /> Nightmare, there is no turning back. Are you brave enough to challenge the darkness? <br />
      Dare to find out...
      </p>
    </div>
    <div>
    <Bar></Bar>
    </div>
    </Container>
    <Container>
    <div className="welc">
         <Nav tit="Reach out to The Enchanted Nightmare"></Nav>
    </div>
    
      <Footer>
      </Footer>
    
    </Container>

      
    </main>
  );
}
