import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import Image from 'next/image';
const bull = (
  <Box
    component="span"
    
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
  </Box>
);

export default function Cajonsito(props:{titulo:string,texto:string,minitext:string,dinero:string,gemas:string,fondo:string}) {
  return (
    
    <Card sx={{ minWidth: 275 }}>
    <div className="fondocajon">
      <CardContent>
        <Typography variant="h4" component="div">
          {props.titulo}
        </Typography>
        <Typography variant="body2">
         {props.texto}
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          {props.minitext}
        </Typography  >
        <Typography className='sus'>
        <Image src ={props.fondo} alt="" width={300}height={300}/>
        </Typography>
      </CardContent>
      <CardActions>
        <Typography sx={{ fontSize: 14 }} color="#122afa" gutterBottom>
          {props.dinero}
        </Typography>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          {props.gemas}
        </Typography >
        <div className="botverd" >
        <Button size="small"  >Book Now</Button>
        </div>
      </CardActions>
      </div>
    </Card>
    
  );
}

